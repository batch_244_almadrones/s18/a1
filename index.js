// 1.  Create a function which will be able to add two numbers.
// 	-Numbers must be provided as arguments.
// 	-Display the result of the addition in our console.
// 	-function should only display result. It should not return anything.
function addition(numA, numB) {
  return numA + numB;
}
// Create a function which will be able to subtract two numbers.
// -Numbers must be provided as arguments.
// -Display the result of subtraction in our console.
// -function should only display result. It should not return anything.
function subtraction(numA, numB) {
  return numA - numB;
}
// -invoke and pass 2 arguments to the addition function
let numA = 5,
  numB = 15;
console.log("Displayed sum of " + numA + " and " + numB + ":");
console.log(addition(numA, numB));
// -invoke and pass 2 arguments to the subtraction function
numA = 20;
numB = 5;
console.log("Displayed difference of " + numA + " and " + numB + ":");
console.log(subtraction(numA, numB));

// 2.  Create a function which will be able to multiply two numbers.
// 		-Numbers must be provided as arguments.
// 		-Return the result of the multiplication.
function multiplication(numA, numB) {
  return numA * numB;
}
// Create a function which will be able to divide two numbers.
// 	-Numbers must be provided as arguments.
// 	-Return the result of the division.
function division(numA, numB) {
  return numA / numB;
}
// Create a global variable called outside of the function called product.
// 	-This product variable should be able to receive and store the result of multiplication function.
numA = 50;
numB = 10;
let product = multiplication(numA, numB);
// Create a global variable called outside of the function called quotient.
// 	-This quotient variable should be able to receive and store the result of division function.

let quotient = division(numA, numB);
// Log the value of product variable in the console.
console.log("Displayed product of " + numA + " and " + numB + ":");
console.log(product);
// Log the value of quotient variable in the console.
console.log("Displayed quotient of " + numA + " and " + numB + ":");
console.log(quotient);

// 3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
// 		-a number should be provided as an argument.
// 		-look up the formula for calculating the area of a circle with a provided/given radius.
// 		-look up the use of the exponent operator.
// 		-you can save the value of the calculation in a variable.
// 		-return the result of the area calculation.
const pi = 3.141592653589793238;
function areaOfCircle(numRadius) {
  return pi * numRadius ** 2;
}
// Create a global variable called outside of the function called circleArea.
// 	-This variable should be able to receive and store the result of the circle area calculation.
numA = 15;
let circleArea = areaOfCircle(numA);
// Log the value of the circleArea variable in the console.
console.log(
  "The result of getting the area of a circle with " + numA + " radius:"
);
console.log(circleArea);

// 4. 	Create a function which will be able to get total average of four numbers.
// 		-4 numbers should be provided as an argument.
// 		-look up the formula for calculating the average of numbers.
// 		-you can save the value of the calculation in a variable.
// 		-return the result of the average calculation.
function numAverage(numA, numB, numC, numD) {
  return (numA + numB + numC + numD) / 4;
}

//     Create a global variable called outside of the function called averageVar.
// 		-This variable should be able to receive and store the result of the average calculation
numA = 20;
numB = 40;
let numC = 60;
let numD = 80;
let averageVar = numAverage(numA, numB, numC, numD);

// 		-Log the value of the averageVar variable in the console.
console.log(
  "The average of " + numA + ", " + numB + ", " + numC + ", and " + numD + ":"
);
console.log(averageVar);

// 	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
// 			-this function should take 2 numbers as an argument, your score and the total score.
// 			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
// 			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
// 			-return the value of the variable isPassed.
// 			-This function should return a boolean.
function examResult(score, totalItems) {
  let rating = (score / totalItems) * 100;
  isPassed = rating >= 75;
  return isPassed;
}

// 		Create a global variable called outside of the function called isPassingScore.
numA = 38;
numB = 50;

let isPassingScore = examResult(numA, numB);
// 			-This variable should be able to receive and store the boolean result of the checker function.
// 			-Log the value of the isPassingScore variable in the console.
console.log("Is " + numA + "/" + numB + " a passing score?");
console.log(isPassingScore);
